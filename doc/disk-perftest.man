.\" Manpage for disk-perftest
.TH man 8 "05 Jun 2024" "1.0" "disk-perftest man page"
.SH NAME
disk-perftest \- Perform a performance test in the current directory
.SH SYNOPSIS
disk-perftest SIZE
.SH DESCRIPTION
disk-perftest writes a file that will have the size specified when being created.

It first creates the file, flushes all cashes and the reads the file.
.SH BUGS
No known bugs.
.SH AUTHOR
Michael Kremser <mkspamx-common@yahoo.de>
