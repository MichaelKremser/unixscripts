# unixscripts

This repository contains various scripts for Unix-like-Systems (including GNU, even if it's not Unix; most scripts would even run on Cygwin and friends). They cover system administation tasks, multimedia tasks and more. These scripts are frequently used by myself and I published them on GitLab (on Github before) to let others benefit and to make installing them on new systems easier for me (believe me, I really often install new systems (mainly GNU/Linux, NetBSD, FreeBSD, and OpenBSD)).

# Installation

You can use git to fetch all of the scripts, wget to download specific scripts (but then you have to track updates yourself) or you can download a Debian package.

## Debian Package

    wget https://gitlab.com/MichaelKremser/unixscripts/-/jobs/7029503916/artifacts/file/build/unixscripts_0.9_all.deb && dpkg -i unixscripts_0.9_all.deb

## git

First you need git software on the system. In Debian and derivates it's enough to install package "git" using `apt-get install git`.

    git clone https://gitlab.com/MichaelKremser/unixscripts.git && cd unixscripts && chmod -v +x *

## wget

You can use any other download program of course, like curl and so on. Even your browser can do a good job. 😉

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/apt-add-dotnet

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/apt-add-virtualbox

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/cert-expiry

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/datewithtz

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/deb-distupgrade-10-11

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/deb-distupgrade-11-12

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/deb-distupgrade-12-13

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/deb-distupgrade-9-10

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/deb-install-essentials

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/dirsizes

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/disk-perftest

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/diskusage

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/do-upgrade

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/emptyswap

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/fix_dotnet_packages

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/fixterm

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/generate-selfsigned-keys

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/git-update-scripts

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/internet-consumption

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/jpeg2mpeg

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/mailserver-start

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/mailserver-stop

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/send-authlog

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/sendhello

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/startwlanap

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/stopwlanap

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/ubu-distupgrade-20-22

    wget https://gitlab.com/MichaelKremser/unixscripts/-/raw/master/ubu-distupgrade-22-24
